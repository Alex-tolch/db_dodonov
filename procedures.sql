CREATE or replace PROCEDURE public.insert_into_usr(IN surnameIn varchar(80), IN nameIn varchar(80), IN patronymicIn varchar(80))
LANGUAGE 'plpgsql'
AS $BODY$
DECLARE records integer;
BEGIN
	SELECT count(*) FROM
		(SELECT * FROM "usr" WHERE "login" = surnameIn || nameIn || patronymicIn) 
		AS Subquery INTO records;
		
	IF records = 0 THEN
		insert into usr ("email", "login", "name", "password", "patronymic", "surname", "telephone_number", "NameUser") 
        values (surnameIn || nameIn || patronymicIn || '@domen.com', surnameIn || nameIn || patronymicIn, nameIn, surnameIn || nameIn || patronymicIn, patronymicIn, surnameIn, 9879206453, DEFAULT);
	ELSE
		RAISE EXCEPTION 'usr % already exists.', department_name;
	END IF;
END;
$BODY$;
ALTER PROCEDURE public.insert_into_usr(character varying, character varying, character varying)
    OWNER TO postgres;

CALL public.insert_into_usr('Иванов', 'Иван', 'Иванович');

select * from usr;


CREATE PROCEDURE public.update_usr(IN loginIn varchar(80), IN oldPassword varchar(80), IN newPassword varchar(80))
LANGUAGE 'plpgsql'
AS $BODY$
DECLARE records integer;
BEGIN
	SELECT count(*) FROM
		(SELECT * FROM "usr" WHERE 
		 "login" = loginIn AND "password" = newPassword) AS Subquery 
		 INTO records;
	
	IF records = 0 THEN 	
		UPDATE "usr" SET id=DEFAULT,
		"password" = newPassword, 
		"NameUser"=DEFAULT
		WHERE "password" = oldPassword;
	ELSE		 
		RAISE EXCEPTION 'Department % already exists.', newName;
	END IF;

END;
$BODY$;
ALTER PROCEDURE public.update_usr(character varying, character varying, character varying)
    OWNER TO postgres;




CREATE or replace PROCEDURE public.delete_from_usr(IN loginIn varchar(80))
LANGUAGE 'plpgsql'
AS $BODY$
DECLARE
records integer;
user_id_orphan integer;
BEGIN

	SELECT count(*) FROM
		(SELECT * FROM "usr" WHERE "login" = loginIn) 
		AS Subquery INTO records;
		
	IF records = 1 THEN
	
		SELECT id from "usr" WHERE "login" = loginIn
		INTO user_id_orphan;
		
		DELETE from "usr" WHERE "login" = loginIn;
		DELETE from "customer" WHERE "user_id" = user_id_orphan;	
	
	END IF;

END
$BODY$;
ALTER PROCEDURE public.delete_from_usr(character varying)
    OWNER TO postgres;
	
select * from usr;

CALL public.delete_from_usr('ИвановИванИванович');