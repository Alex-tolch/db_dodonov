CREATE VIEW public."view_for_trigger"
 AS
 SELECT subquery.id,
    subquery."Name"
   FROM ( SELECT "status".id, 
            "status".status_name
           FROM "status") subquery;

ALTER TABLE public."view_for_trigger"
    OWNER TO postgres;


CREATE FUNCTION public.view_for_trigger_func()
    RETURNS trigger
    LANGUAGE 'plpgsql'
     NOT LEAKPROOF
AS $BODY$
BEGIN
	IF (TG_OP = 'DELETE') THEN
            DELETE FROM "status" WHERE "id" = OLD."id";
    IF NOT FOUND 
        THEN RETURN NULL; 
    END IF;
RETURN OLD;

    ELSIF (TG_OP = 'UPDATE') THEN
            UPDATE "status" SET "id" = NEW."id"
            WHERE "id" = OLD."id";
    IF NOT FOUND 
        THEN RETURN NULL; 
    END IF;
        RETURN NEW;
			
    ELSIF (TG_OP = 'INSERT') THEN
        INSERT INTO "status"("status_name") VALUES(NEW."status_name");
        RETURN NEW;
    END IF;

END;
$BODY$;

ALTER FUNCTION public.view_for_trigger_func()
    OWNER TO postgres;


CREATE TRIGGER view_for_trigger_trigger
    INSTEAD OF INSERT OR DELETE OR UPDATE 
    ON public."view_for_trigger"
    FOR EACH ROW
    EXECUTE FUNCTION public.view_for_trigger_func();
