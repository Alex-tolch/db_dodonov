CREATE SEQUENCE public."usr_id_seq"
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1
    OWNED BY "usr".id;

ALTER SEQUENCE public."usr_id_seq"
    OWNER TO postgres;


CREATE SEQUENCE public."status_id_seq"
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1
    OWNED BY "status".id;

ALTER SEQUENCE public."status_id_seq"
    OWNER TO postgres;


CREATE SEQUENCE public."results_id_seq"
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1
    OWNED BY "results".id;

ALTER SEQUENCE public."results_id_seq"
    OWNER TO postgres;


CREATE SEQUENCE public."frame_id_seq"
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1
    OWNED BY "frame".id;

ALTER SEQUENCE public."frame_id_seq"
    OWNER TO postgres;


CREATE SEQUENCE public."customer_id_seq"
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1
    OWNED BY "customer".id;

ALTER SEQUENCE public."customer_id_seq"
    OWNER TO postgres;


CREATE SEQUENCE public."calculation_id_seq"
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1
    OWNED BY "calculation".id;

ALTER SEQUENCE public."calculation_id_seq"
    OWNER TO postgres;

