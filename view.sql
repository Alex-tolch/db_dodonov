CREATE OR REPLACE VIEW test_view0 AS select 
 r.id,
 r.amount,
 r.price,
 r.total_cost,
 c.construction_address,
 f.base_area,
 f.floor_number
from
  results r 
  left outer join frame f on r.frame_id = f.id
  left outer join calculation c ON c.id = r.calculation_id;

create or replace view test_view1 as select * from usr;
create or replace view test_view2 as select * from usr order by id desc;

create or replace view test_view3 as select u.id, u.surname || ' ' || u.name || ' ' || u.patronymic as manager, 
u.login as manager_login, 
c.surname || ' ' || c.name || ' ' || c.patronymic as customer 
from usr u left join customer c on u.id = c.user_id;

create or replace view test_view4 as select u.id, u.surname || ' ' || u.name || ' ' || u.patronymic as manager, 
u.login as manager_login, 
c.surname || ' ' || c.name || ' ' || c.patronymic as customer 
from usr u right join customer c on u.id = c.user_id;

create or replace view test_view5 as select u.id, u.surname || ' ' || u.name || ' ' || u.patronymic as manager, 
u.login as manager_login, 
c.surname || ' ' || c.name || ' ' || c.patronymic as customer 
from usr u join customer c on u.id = c.user_id;

create or replace view test_view6 as select u.id, u.surname || ' ' || u.name || ' ' || u.patronymic as manager, 
u.login as manager_login, 
c.surname || ' ' || c.name || ' ' || c.patronymic as customer 
from usr u left outer join customer c on u.id = c.user_id;

create or replace view test_view7 as select u.id, u.surname || ' ' || u.name || ' ' || u.patronymic as manager, 
u.login as manager_login, 
c.surname || ' ' || c.name || ' ' || c.patronymic as customer 
from usr u inner join customer c on u.id = c.user_id;

create or replace view test_view8 as select * from results;
create or replace view test_view9 as select * from results order by customer_id desc;

create or replace view test_view10 as select * from frame;
create or replace view test_view11 as select * from frame order by number_of_floors desc;

create or replace view test_view12 as select * from calculation;
create or replace view test_view13 as select * from calculation order by data_of_creation desc;

create or replace view test_view14 as select * from customer;
create or replace view test_view15 as select * from customer order by telephone_number desc;

create or replace view test_view16 as select c.construction_address, c.data_of_creation, c.number, r.amount, r.price, f.floor_number 
from calculation c 
join results r on c.id = r.calculation_id
join frame f on f.id = r.frame_id;

create or replace view test_view17 as select c.construction_address, c.data_of_creation, c.number, r.amount, r.price, f.floor_number 
from calculation c 
left join results r on c.id = r.calculation_id
left join frame f on f.id = r.frame_id;

create or replace view test_view18 as select c.construction_address, c.data_of_creation, c.number, r.amount, r.price, f.floor_number 
from calculation c 
left outer join results r on c.id = r.calculation_id
left outer join frame f on f.id = r.frame_id;

create or replace view test_view19 as select c.construction_address, c.data_of_creation, c.number, r.amount, r.price, f.floor_number 
from calculation c 
right outer join results r on c.id = r.calculation_id
right outer join frame f on f.id = r.frame_id;

create or replace view test_view20 as select c.construction_address, c.data_of_creation, c.number, r.amount, r.price, f.floor_number 
from calculation c 
inner join results r on c.id = r.calculation_id
inner join frame f on f.id = r.frame_id;


ALTER TABLE IF EXISTS usr ADD COLUMN "NameUser" character varying(50) DEFAULT USER;
ALTER TABLE IF EXISTS calculation ADD COLUMN "NameUser" character varying(50) DEFAULT USER;
ALTER TABLE IF EXISTS customer ADD COLUMN "NameUser" character varying(50) DEFAULT USER;
ALTER TABLE IF EXISTS frame ADD COLUMN "NameUser" character varying(50) DEFAULT USER;
ALTER TABLE IF EXISTS status ADD COLUMN "NameUser" character varying(50) DEFAULT USER;
ALTER TABLE IF EXISTS results ADD COLUMN "NameUser" character varying(50) DEFAULT USER;


CREATE VIEW Updatable_customer_u
AS SELECT id AS unic_number, address, email, telephone_number
FROM customer WHERE "NameUser" = USER;

CREATE VIEW Updatable_calculation_u
AS SELECT  construction_address, data_of_creation, number
FROM calculation WHERE "NameUser" = USER;

CREATE VIEW Updatable_frame_u
AS SELECT  base_area, floor_height, floor_number, internal_wall_length, number_of_floors
FROM frame WHERE "NameUser" = USER;

CREATE VIEW Updatable_results_u
AS SELECT  amount, part_of_floor, total_cost
FROM results WHERE "NameUser" = USER;

CREATE VIEW Updatable_status_u
AS SELECT  status_name
FROM status WHERE "NameUser" = USER;

CREATE VIEW Updatable_usr_u
AS SELECT  email, login, telephone_number
FROM usr WHERE "NameUser" = USER;


create view Updatable_customer_1 as
select c.construction_address, c.data_of_creation, c.number, r.amount, r.price, f.floor_number 
from calculation c 
join results r on c.id = r.calculation_id
join frame f on f.id = r.frame_id;

create view Updatable_customer_2 as
select c.construction_address, c.data_of_creation, c.number, r.amount, r.price, f.floor_number 
from calculation c 
left join results r on c.id = r.calculation_id
left join frame f on f.id = r.frame_id;

create view Updatable_customer_3 as
select c.construction_address, c.data_of_creation, c.number, r.amount, r.price, f.floor_number 
from calculation c 
left outer join results r on c.id = r.calculation_id
left outer join frame f on f.id = r.frame_id;

create view Updatable_customer_4 as
select c.construction_address, c.data_of_creation, c.number, r.amount, r.price, f.floor_number 
from calculation c 
right outer join results r on c.id = r.calculation_id
right outer join frame f on f.id = r.frame_id;

create view Updatable_customer_5 as
select c.construction_address, c.data_of_creation, c.number, r.amount, r.price, f.floor_number 
from calculation c 
inner join results r on c.id = r.calculation_id
inner join frame f on f.id = r.frame_id;

create view Updatable_customer_6 as
select * from calculation;
select * from calculation order by data_of_creation desc;
