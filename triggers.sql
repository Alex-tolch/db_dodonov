CREATE FUNCTION public.usr_before_add()
    RETURNS trigger
    LANGUAGE 'plpgsql'
     NOT LEAKPROOF
AS $BODY$
BEGIN
	NEW.id := nextval('"usr_id_seq"');
	RETURN NEW;
END;
$BODY$;

ALTER FUNCTION public.usr_before_add()
    OWNER TO postgres;

CREATE TRIGGER usr_before_add_trigger
    BEFORE INSERT OR UPDATE 
    ON public."usr"
    FOR EACH ROW
    EXECUTE FUNCTION public.usr_before_add();


CREATE FUNCTION public.status_before_add()
    RETURNS trigger
    LANGUAGE 'plpgsql'
     NOT LEAKPROOF
AS $BODY$
BEGIN
	NEW.id := nextval('"status_id_seq"');
	RETURN NEW;
END;
$BODY$;

ALTER FUNCTION public.status_before_add()
    OWNER TO postgres;

CREATE TRIGGER status_before_add_trigger
    BEFORE INSERT OR UPDATE 
    ON public."status"
    FOR EACH ROW
    EXECUTE FUNCTION public.status_before_add();


CREATE FUNCTION public.results_before_add()
    RETURNS trigger
    LANGUAGE 'plpgsql'
     NOT LEAKPROOF
AS $BODY$
BEGIN
	NEW.id := nextval('"results_id_seq"');
	RETURN NEW;
END;
$BODY$;

ALTER FUNCTION public.results_before_add()
    OWNER TO postgres;

CREATE TRIGGER results_before_add_trigger
    BEFORE INSERT OR UPDATE 
    ON public."results"
    FOR EACH ROW
    EXECUTE FUNCTION public.results_before_add();


CREATE FUNCTION public.frame_before_add()
    RETURNS trigger
    LANGUAGE 'plpgsql'
     NOT LEAKPROOF
AS $BODY$
BEGIN
	NEW.id := nextval('"frame_id_seq"');
	RETURN NEW;
END;
$BODY$;

ALTER FUNCTION public.frame_before_add()
    OWNER TO postgres;

CREATE TRIGGER frame_before_add_trigger
    BEFORE INSERT OR UPDATE 
    ON public."frame"
    FOR EACH ROW
    EXECUTE FUNCTION public.frame_before_add();


CREATE FUNCTION public.customer_before_add()
    RETURNS trigger
    LANGUAGE 'plpgsql'
     NOT LEAKPROOF
AS $BODY$
BEGIN
	NEW.id := nextval('"customer_id_seq"');
	RETURN NEW;
END;
$BODY$;

ALTER FUNCTION public.customer_before_add()
    OWNER TO postgres;

CREATE TRIGGER customer_before_add_trigger
    BEFORE INSERT OR UPDATE 
    ON public."customer"
    FOR EACH ROW
    EXECUTE FUNCTION public.customer_before_add();


CREATE FUNCTION public.calculation_before_add()
    RETURNS trigger
    LANGUAGE 'plpgsql'
     NOT LEAKPROOF
AS $BODY$
BEGIN
	NEW.id := nextval('"calculation_id_seq"');
	RETURN NEW;
END;
$BODY$;

ALTER FUNCTION public.calculation_before_add()
    OWNER TO postgres;

CREATE TRIGGER calculation_before_add_trigger
    BEFORE INSERT OR UPDATE 
    ON public."calculation"
    FOR EACH ROW
    EXECUTE FUNCTION public.calculation_before_add();


insert into usr (email, login, name, password, patronymic, surname, telephone_number) values ('userlogin@mail.ru', 'userlogin888', 'serik', 'megass228', 'maratovich', 'maratov', 9879236453);

insert into status (status_name) values ('deactive');

insert into results (amount, calculation_id, frame_id, part_of_floor, price, total_cost) values (10, 0, 0, 'outdoor wall', 10000, 100000);

insert into frame (base_area, external_wall_thickness, floor_height, floor_number, internal_wall_length, internal_wall_thickness, number_of_floors, outer_wall_perimeter, overlap_thickness) values (100, 0.15, 3, 1, 10, 0.15, 3, 40, 0.15);

insert into customer (address, email, name, patronymic, surname, telephone_number, user_id) values ('revolutsionnaya 64, samara', 'customerlogin@gmail.ru', 'otec_alexiy', 'mafioznik', 'vor_v_sakone', 8005553535, 0);

insert into calculation (construction_address, data_of_creation, number, status_id) values ('moskovskoe shosse 34b, samara', current_date, '46863292-de14-11ed-b5ea-0242ac120002', 0);









CREATE FUNCTION public.status_before_delete()
    RETURNS trigger
    LANGUAGE 'plpgsql'
     NOT LEAKPROOF
AS $BODY$
BEGIN

	UPDATE public."calculation"
		SET status_id=NULL, "NameUser"=DEFAULT
	WHERE status_id=OLD.id;
	
	RETURN OLD;
	
END;
$BODY$;

ALTER FUNCTION public.status_before_delete()
    OWNER TO postgres;

CREATE TRIGGER status_before_delete_trigger
    BEFORE DELETE
    ON public."status"
    FOR EACH ROW
    EXECUTE FUNCTION public.status_before_delete();



CREATE FUNCTION public.usr_before_delete()
    RETURNS trigger
    LANGUAGE 'plpgsql'
     NOT LEAKPROOF
AS $BODY$
BEGIN

	UPDATE public."customer"
		SET user_id=NULL, "NameUser"=DEFAULT
	WHERE user_id=OLD.id;
	
	RETURN OLD;
	
END;
$BODY$;

ALTER FUNCTION public.usr_before_delete()
    OWNER TO postgres;

CREATE TRIGGER usr_before_delete_trigger
    BEFORE DELETE
    ON public."usr"
    FOR EACH ROW
    EXECUTE FUNCTION public.usr_before_delete();



CREATE FUNCTION public.customer_before_delete()
    RETURNS trigger
    LANGUAGE 'plpgsql'
     NOT LEAKPROOF
AS $BODY$
BEGIN

	UPDATE public."calculation"
		SET customer_id=NULL, "NameUser"=DEFAULT
	WHERE customer_id=OLD.id;
	
	RETURN OLD;
	
END;
$BODY$;

ALTER FUNCTION public.customer_before_delete()
    OWNER TO postgres;

CREATE TRIGGER customer_before_delete_trigger
    BEFORE DELETE
    ON public."customer"
    FOR EACH ROW
    EXECUTE FUNCTION public.customer_before_delete();



CREATE FUNCTION public.frame_before_delete()
    RETURNS trigger
    LANGUAGE 'plpgsql'
     NOT LEAKPROOF
AS $BODY$
BEGIN

	UPDATE public."results"
		SET frame_id=NULL, "NameUser"=DEFAULT
	WHERE frame_id=OLD.id;
	
	RETURN OLD;
	
END;
$BODY$;

ALTER FUNCTION public.frame_before_delete()
    OWNER TO postgres;

CREATE TRIGGER frame_before_delete_trigger
    BEFORE DELETE
    ON public."frame"
    FOR EACH ROW
    EXECUTE FUNCTION public.frame_before_delete();



CREATE FUNCTION public.calculation_before_delete()
    RETURNS trigger
    LANGUAGE 'plpgsql'
     NOT LEAKPROOF
AS $BODY$
BEGIN

	UPDATE public."results"
		SET calculation_id=NULL, "NameUser"=DEFAULT
	WHERE calculation_id=OLD.id;
	
	RETURN OLD;
	
END;
$BODY$;

ALTER FUNCTION public.calculation_before_delete()
    OWNER TO postgres;

CREATE TRIGGER calculation_before_delete_trigger
    BEFORE DELETE
    ON public."calculation"
    FOR EACH ROW
    EXECUTE FUNCTION public.calculation_before_delete();










CREATE OR REPLACE FUNCTION public.usr_before_add()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    VOLATILE
    COST 100
AS $BODY$
DECLARE records integer;
BEGIN
	SELECT count(*) FROM
		(SELECT * FROM "usr" WHERE "email" = NEW."email") 
AS Subquery INTO records;
		
	IF records > 0 THEN
		RAISE EXCEPTION 'usr % already exists.', NEW."email";
	END IF;

	NEW.id := nextval('"usr_id_seq"');
	RETURN NEW;
END;
$BODY$;



CREATE OR REPLACE FUNCTION public.customer_before_add()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    VOLATILE
    COST 100
AS $BODY$
DECLARE records integer;
BEGIN
	SELECT count(*) FROM
		(SELECT * FROM "customer" WHERE "Name" = NEW."Name") AS Subquery INTO records;
		
	IF records > 0 THEN
		RAISE EXCEPTION 'customer % already exists.', NEW."Name";
	END IF;

	NEW.id := nextval('"customer_id_seq"');
	RETURN NEW;
END;
$BODY$;



CREATE OR REPLACE FUNCTION public.calculation_before_add()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    VOLATILE
    COST 100
AS $BODY$
DECLARE records integer;
BEGIN
	SELECT count(*) FROM
		(SELECT * FROM "calculation" WHERE "construction_address" = NEW."construction_address") AS Subquery INTO records;
		
	IF records > 0 THEN
		RAISE EXCEPTION 'calculation % already exists.', NEW."construction_address";
	END IF;

	NEW.id := nextval('"calculation_id_seq"');
	RETURN NEW;
END;
$BODY$;








