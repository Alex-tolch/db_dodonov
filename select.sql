select * from usr;
select * from usr order by id desc;

select u.id, u.surname || ' ' || u.name || ' ' || u.patronymic as manager, 
u.login as manager_login, 
c.surname || ' ' || c.name || ' ' || c.patronymic as customer 
from usr u left join customer c on u.id = c.user_id;

select u.id, u.surname || ' ' || u.name || ' ' || u.patronymic as manager, 
u.login as manager_login, 
c.surname || ' ' || c.name || ' ' || c.patronymic as customer 
from usr u right join customer c on u.id = c.user_id;

select u.id, u.surname || ' ' || u.name || ' ' || u.patronymic as manager, 
u.login as manager_login, 
c.surname || ' ' || c.name || ' ' || c.patronymic as customer 
from usr u join customer c on u.id = c.user_id;

select u.id, u.surname || ' ' || u.name || ' ' || u.patronymic as manager, 
u.login as manager_login, 
c.surname || ' ' || c.name || ' ' || c.patronymic as customer 
from usr u left outer join customer c on u.id = c.user_id;

select u.id, u.surname || ' ' || u.name || ' ' || u.patronymic as manager, 
u.login as manager_login, 
c.surname || ' ' || c.name || ' ' || c.patronymic as customer 
from usr u inner join customer c on u.id = c.user_id;

select * from results;
select * from results order by customer_id desc;

select * from frame;
select * from frame order by number_of_floors desc;

select * from calculation;
select * from calculation order by data_of_creation desc;

select * from customer;
select * from customer order by telephone_number desc;

select c.construction_address, c.data_of_creation, c.number, r.amount, r.price, f.floor_number 
from calculation c 
join results r on c.id = r.calculation_id
join frame f on f.id = r.frame_id;

select c.construction_address, c.data_of_creation, c.number, r.amount, r.price, f.floor_number 
from calculation c 
left join results r on c.id = r.calculation_id
left join frame f on f.id = r.frame_id;

select c.construction_address, c.data_of_creation, c.number, r.amount, r.price, f.floor_number 
from calculation c 
left outer join results r on c.id = r.calculation_id
left outer join frame f on f.id = r.frame_id;

select c.construction_address, c.data_of_creation, c.number, r.amount, r.price, f.floor_number 
from calculation c 
right outer join results r on c.id = r.calculation_id
right outer join frame f on f.id = r.frame_id;

select c.construction_address, c.data_of_creation, c.number, r.amount, r.price, f.floor_number 
from calculation c 
inner join results r on c.id = r.calculation_id
inner join frame f on f.id = r.frame_id;

