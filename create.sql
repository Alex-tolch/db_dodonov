CREATE TABLE calculation (
    id SERIAL NOT NULL, 
    construction_address varchar(255), 
    data_of_creation timestamp, 
    number uuid, 
    status_id int8, 
    customer_id int8, 
    primary key (id)
);

CREATE TABLE customer (
    id SERIAL NOT NULL, 
    address varchar(255), 
    email varchar(255), 
    name varchar(255), 
    patronymic varchar(255), 
    surname varchar(255), 
    telephone_number int8 NOT NULL, 
    user_id int8, 
    primary key (id)
);

CREATE TABLE frame (
    id SERIAL NOT NULL, 
    base_area float8 NOT NULL, 
    external_wall_thickness float8 NOT NULL, 
    floor_height int4 NOT NULL, 
    floor_number int4 NOT NULL, 
    internal_wall_length float8 NOT NULL, 
    internal_wall_thickness float8 NOT NULL, 
    number_of_floors int4 NOT NULL, 
    outer_wall_perimeter float8 NOT NULL, 
    overlap_thickness float8, 
    primary key (id)
);

CREATE TABLE results (
    id SERIAL NOT NULL, 
    amount float8 NOT NULL, 
    calculation_id int8 NOT NULL,
    part_of_floor varchar(255), 
    price float8 NOT NULL, 
    total_cost float8 NOT NULL, 
    frame_id int8, 
    primary key (id)
);

CREATE TABLE status (
    id SERIAL NOT NULL, 
    status_name varchar(255), 
    primary key (id)
);

CREATE TABLE usr (
    id SERIAL NOT NULL, 
    email varchar(255), 
    login varchar(255) NOT NULL UNIQUE, 
    name varchar(255), 
    password varchar(255), 
    patronymic varchar(255), 
    surname varchar(255), 
    telephone_number int8 NOT NULL, 
    primary key (id)
);

ALTER TABLE IF EXISTS calculation ADD CONSTRAINT FK632907els7n47nyivpel6bqkp FOREIGN KEY (status_id) REFERENCES status;
ALTER TABLE IF EXISTS calculation ADD CONSTRAINT FK2uqoce08jfi13f6ua4wbfkkb0 FOREIGN KEY (customer_id) REFERENCES customer;
ALTER TABLE IF EXISTS customer ADD CONSTRAINT FKptv929uyoreysdjm9aqaofnx5 FOREIGN KEY (user_id) REFERENCES usr;
ALTER TABLE IF EXISTS results ADD CONSTRAINT FKjutkowrxt8pvxacwmg7g5je7h FOREIGN KEY (frame_id) REFERENCES frame;
ALTER TABLE IF EXISTS results ADD CONSTRAINT FKjhmpks2jitrje92287e3f93y6 FOREIGN KEY (calculation_id) REFERENCES calculation;
